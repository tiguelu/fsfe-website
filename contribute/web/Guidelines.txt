= Web Guidelines =
******************

This document aims at giving webmasters and people who contribute to write
webpages a set of guidelines to help them making the publishing process as good
as possible.

[Note: this guideline is about regular publication. Any work involved with the
global restructuration of the website has to comply with another set of
guidelines discussed among webmasters@fsfeurope.org -- see below.]

== Technical structure ==

Information on how to contribute to the website are described online at:
http://fsfe.org/contribute/web/web.en.html

The website is composed of several SVN branches.
 * ''Trunk'' is the website published on fsfe.org
 * ''Test'' is accessible on test.fsfe.org

== Publishing a new page ==

When you create a new page, you can create it directly in Trunk if the following
conditions are met:
 * the new page has nothing to do with the restructuration of the website
 * it's not a replacement or an equivalent of an existing document
 * no information is duplicated from another place (even outside FSFE)
 * it does not use fancy style properties

Make sure that the page is published in the appropriate directory.

If your page does not meet the precedent requirements, then you have to submit
your proposition to webmasters@fsfeurope.org 

== Publishing modifications of a page ==

If you are modifying an existing page, please make sure that you have the last
version by updating your SVN repository.

 * Are new translations needed?
   When you modify an existing document, if it is a minor change or just a
   correction (e.g. a mispell, a typo, a wrong wording, an html problem) then
   you might not need a translation. In order to ease the translation work, do
   not ping for translations when it's not needed. [FIXME: How can we do that?]

 * Is the web page important information?
   If the web page you are modifying is an important page, please run through
   the editor process before publishing the page.

 * Are you making important changes?
   If the changes you bring to the page are very important, don't hesitate to
   email the original author. Also, publishing in ''Test'' before publishing in
   Trunk might be useful to get feedback from several readers.

 * Does it have fancy style properties?
   If yes, please use the ''Test'' branch before publishing in ''Trunk''.
   
== Source Code format ==

 * Line breaks
   It is important that each document has line breaks at 80 char. This is needed
   to ease translations and diffs. 
   
 * Mark-ups

== Rules for content ==

 * Punctuation
 * Wordlist
 * Catchphrases

== Translations ==

== Editors ==

== Tools ==
 * Browse, diffs, timeline: https://trac.fsfe.org/fsfe-web/browser/trunk
